# -*- coding:utf-8 -*-
from annoying.functions import get_object_or_None
from django.contrib.auth import get_user_model


class EmailUserBackend(object):
    def authenticate(self, email=None, password=None):
        model = get_user_model()
        try:
            user = model.objects.filter(email=email).first()
        except model.DoesNotExist as e:
            return None

        if not user.check_password(password):
            return None

        return user

    def get_user(self, user_id):
        model = get_user_model()
        user = get_object_or_None(model, email=user_id)
        return user