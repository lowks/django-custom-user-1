# Django Custom User  #

* Django Application
* Support the latest Django version
* Provide Custom Email User (with username included)

# Installation #

    pip install anderson.django-custom-user

# How to Setup #

### EmailUserModel ###

You need to subclass EmailUserModel first. 


```
#!python
from custom_auth.models import EmailAbstractUser

class MyUserModel(EmailAbstractUser):
    class Meta:
        db_table = 'custom_user'

```

### Settings.py ###

If you want to use Custom Email User (with username included), AUTHENTICATION_BACKENDS in settings.py should be the same as the following. 

Make AUTH_USER_MODEL reference to the model that subclasses EmailAbstractUser class.

```
#!python

AUTHENTICATION_BACKENDS = ['custom_auth.backend.EmailUserBackend'] 
AUTH_USER_MODEL = 'your_application.MyUserModel'
```


### EmailUserCreationForm ###

You can use EmailUserCreationForm to register a user instead of using UserCreationForm.


```
#!python

from custom_auth.forms import EmailUserCreationForm
data = {}
data['username'] = 'tester024'
data['email'] = 'tester4353@gmail.com'
data['password1'] = '1234'
data['password2'] = '1234'

form = EmailUserCreationForm(data)
form.is_valid() # True
form.save()
```

# Tools #

### user_exists function ###

You can check if the email exists. 

    user_exists('test@google.com') # True or False

### MYSQL 150 Error ###

When you run 'syncdb' Mysql could raise 150 Error.

    _mysql_exceptions.OperationalError: (
        1005, "Can't create table '\\db_name\\.#sql-4a8_ab' (errno: 150)"
    )

The error is related to the database engine. 

See the following link. 

https://docs.djangoproject.com/en/1.8/ref/databases/#storage-engines

You need to set database engine. 



```
#!python

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "db_name",
        "USER": "username",
        "PASSWORD": "password",
        "HOST": "localhost",
        "PORT": 3306,
        "OPTIONS": {
            "init_command": "SET storage_engine=MyISAM"
        }
    }
}
```